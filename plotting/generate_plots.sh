#! /bin/bash
org_pwd=`pwd`
hipSYCL_hash=$1

echo -e "\e[1;96mNormalizing output\e[0m"
echo -e "\e[1;94m  sycl-bench\e[0m"
SYCL_BENCH_CSV_FILE=${org_pwd}/res/sycl-bench-red-${hipSYCL_hash}.csv
sed -n '1p' ${org_pwd}/res/sycl-bench-red-cbs-${hipSYCL_hash}.csv | sed 's/# /pipeline,/' > ${SYCL_BENCH_CSV_FILE}
grep "Hierarchical" ${org_pwd}/res/sycl-bench-red-cbs-${hipSYCL_hash}.csv | sed 's/^/hierarchical,/' | sed 's/Pattern_Reduction_Hierarchical_//' >> ${SYCL_BENCH_CSV_FILE}
grep "NDRange" ${org_pwd}/res/sycl-bench-red-org-${hipSYCL_hash}.csv | sed 's/^/org,/' | sed 's/Pattern_Reduction_NDRange_//' >> ${SYCL_BENCH_CSV_FILE}
grep "NDRange" ${org_pwd}/res/sycl-bench-red-pocl-${hipSYCL_hash}.csv | sed 's/^/pocl,/' | sed 's/Pattern_Reduction_NDRange_//' >> ${SYCL_BENCH_CSV_FILE}
grep "NDRange" ${org_pwd}/res/sycl-bench-red-cbs-${hipSYCL_hash}.csv | sed 's/^/cbs,/' | sed 's/Pattern_Reduction_NDRange_//' >> ${SYCL_BENCH_CSV_FILE}
grep "NDRange" ${org_pwd}/res/sycl-bench-red-fibers.csv | sed 's/^/fibers,/' | sed 's/Pattern_Reduction_NDRange_//' >> ${SYCL_BENCH_CSV_FILE}

echo -e "\e[1;94m  sycl-blas\e[0m"
SYCL_BLAS_CSV_FILE=${org_pwd}/res/sycl-blas-gemv-${hipSYCL_hash}.csv
grep "name,iterations" ${org_pwd}/res/sycl-blas-gemv-cbs-${hipSYCL_hash}.csv | sed 's/$/,pipeline/' > ${SYCL_BLAS_CSV_FILE}
sed -e '1,10d' ${org_pwd}/res/sycl-blas-gemv-org-${hipSYCL_hash}.csv | sed 's/$/,org/' >> ${SYCL_BLAS_CSV_FILE}
sed -e '1,10d' ${org_pwd}/res/sycl-blas-gemv-pocl-${hipSYCL_hash}.csv | sed 's/$/,pocl/' >> ${SYCL_BLAS_CSV_FILE}
sed -e '1,10d' ${org_pwd}/res/sycl-blas-gemv-cbs-${hipSYCL_hash}.csv | sed 's/$/,cbs/' >> ${SYCL_BLAS_CSV_FILE}
sed -e '1,11d' ${org_pwd}/res/sycl-blas-gemv-fibers.csv | sed 's/$/,fiber/' >> ${SYCL_BLAS_CSV_FILE}

echo -e "\e[1;94m  PRK\e[0m"
PRK_CSV_FILE=${org_pwd}/res/prk-dgemm-${hipSYCL_hash}.csv
echo "pipeline,iterations,order,tile,mfs,average" > ${PRK_CSV_FILE}

echo -n "pocl," >> ${PRK_CSV_FILE}                  
cat ${org_pwd}/res/prk-dgemm-1024-pocl-${hipSYCL_hash}.txt | grep -E "(\):| =)" | grep -o -E '([0-9]|\.)+' | tr '\n' ',' | sed '$s/,$/\n/' >> ${PRK_CSV_FILE}
echo -n "pocl," >> ${PRK_CSV_FILE}
cat ${org_pwd}/res/prk-dgemm-4096-pocl-${hipSYCL_hash}.txt | grep -E "(\):| =)" | grep -o -E '([0-9]|\.)+' | tr '\n' ',' | sed '$s/,$/\n/' >> ${PRK_CSV_FILE}

echo -n "cbs," >> ${PRK_CSV_FILE}                  
cat ${org_pwd}/res/prk-dgemm-1024-cbs-${hipSYCL_hash}.txt | grep -E "(\):| =)" | grep -o -E '([0-9]|\.)+' | tr '\n' ',' | sed '$s/,$/\n/' >> ${PRK_CSV_FILE}
echo -n "cbs," >> ${PRK_CSV_FILE}
cat ${org_pwd}/res/prk-dgemm-4096-cbs-${hipSYCL_hash}.txt | grep -E "(\):| =)" | grep -o -E '([0-9]|\.)+' | tr '\n' ',' | sed '$s/,$/\n/' >> ${PRK_CSV_FILE}

echo -n "fibers," >> ${PRK_CSV_FILE}                  
cat ${org_pwd}/res/prk-dgemm-1024-fibers.txt | grep -E "(\):| =)" | grep -o -E '([0-9]|\.)+' | tr '\n' ',' | sed '$s/,$/\n/' >> ${PRK_CSV_FILE}
echo -n "fibers," >> ${PRK_CSV_FILE}
cat ${org_pwd}/res/prk-dgemm-4096-fibers.txt | grep -E "(\):| =)" | grep -o -E '([0-9]|\.)+' | tr '\n' ',' | sed '$s/,$/\n/' >> ${PRK_CSV_FILE}

echo -e "\e[1;94m  BabelStream\e[0m"
BABEL_STREAM_CSV_FILE=${org_pwd}/res/BabelStream-${hipSYCL_hash}.csv
grep "function,num_times" ${org_pwd}/res/BabelStream-cbs-${hipSYCL_hash}.csv | sed 's/$/,pipeline/' > ${BABEL_STREAM_CSV_FILE}
grep "Dot" ${org_pwd}/res/BabelStream-org-${hipSYCL_hash}.csv | sed 's/$/,org/' >> ${BABEL_STREAM_CSV_FILE}
grep "Dot" ${org_pwd}/res/BabelStream-pocl-${hipSYCL_hash}.csv | sed 's/$/,pocl/' >> ${BABEL_STREAM_CSV_FILE}
grep "Dot" ${org_pwd}/res/BabelStream-cbs-${hipSYCL_hash}.csv | sed 's/$/,cbs/' >> ${BABEL_STREAM_CSV_FILE}
grep "Dot" ${org_pwd}/res/BabelStream-fibers.csv | sed 's/$/,fibers/' >> ${BABEL_STREAM_CSV_FILE}

echo -e "\e[1;96mGerating plots for ${1}\e[0m"
python ${org_pwd}/plotting/plot.py plot_sycl_bench ${SYCL_BENCH_CSV_FILE} --index_col=dtype --yscale=log --fig_size="(6,5)" --kind=bar

python ${org_pwd}/plotting/plot.py plot_prk ${PRK_CSV_FILE} --index_col=order --yscale=log --fig_size="(6,5)" --kind=bar
python ${org_pwd}/plotting/plot.py plot_prk ${PRK_CSV_FILE} --index_col=order --yscale=log --fig_size="(6,5)" --kind=bar --MF

python ${org_pwd}/plotting/plot.py plot_sycl_blas ${SYCL_BLAS_CSV_FILE} --yscale=log --kind=bar --fig_size="(6,5)"

python ${org_pwd}/plotting/plot.py plot_babel_stream ${BABEL_STREAM_CSV_FILE} --index_col=function --throughput --yscale=log --kind=bar --fig_size="(6,5)"

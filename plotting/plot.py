import matplotlib.pyplot as plt
import matplotlib as mpl
import fire
import pandas
import numpy as np
import os, sys
import seaborn as sns

def set_markers_for_axis(ax):
    markers = ([item[0] for item in mpl.markers.MarkerStyle.markers.items() if 
        item[1] != 'nothing' and not item[1].startswith('tick') 
        and not item[1].startswith('caret') and not item[0] == ','])
    for i, line in enumerate(ax.get_lines()):
        print(markers[i])
        line.set_marker(markers[i])

class cli:
    @staticmethod
    def plot_catch(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', show_all_ticks : bool = False, fig_size = (5,5)):
        """
        Plots a Catch2 benchmark csv file.
        
        :param file: Input file.
        :param index_col: The name of the captured C++ variable to use for the x axis. Can also be a list (e.g. "['numBlocks','threadsPerBlock']").
        :param xscale: linear or log (or whatever matplotlib allows)
        :param yscale: linear or log (or whatever matplotlib allows)
        :param kind: line (or whatever matplotlib allows)
        """
        df = pandas.read_csv(csv, sep="\t", header=None)
        name = {}
        def extract_value(value, col):
            vals = value.split(" := ")
            if len(vals) > 1:
                name[col] = vals[0]
                if "(" in vals[1]: # get rid of (0x..)
                    vals[1] = vals[1][0:vals[1].index("(")]
                try:
                    return int(vals[1])
                except ValueError:
                    return vals[1]

            return vals[0]

        for i in range(2, len(df.columns)):
            df[i] = df[i].apply(extract_value, True, (i,))

        name[0] = "benchmark"
        name[1] = "time"
        df.rename(columns=name, inplace=True)
        df = df.pivot_table(index=index_col, columns='benchmark', values='time')

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        ax.set_ylabel("time [ns]")
        df.plot(kind=kind, ax=ax)

        if show_all_ticks:
            tick_names = df.index.to_list()
            ax.set_xticks(np.arange(len(tick_names)))
            ax.set_xticklabels(df.index.to_list(), rotation=90)
            # fig.subplots_adjust(bottom=0.2)
        plt.tight_layout()
        
        if kind == 'line':
            set_markers_for_axis(ax)
        
        plt.savefig(csv + ".png")

    @staticmethod
    def plot_google(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', show_all_ticks : bool = False, fig_size = (11,5)):
        """
        Plots a google benchmark csv file.
        
        :param file: Input file.
        :param index_col: The name of the captured C++ variable to use for the x axis. Can also be a list (e.g. "['numBlocks','threadsPerBlock']").
        :param xscale: linear or log (or whatever matplotlib allows)
        :param yscale: linear or log (or whatever matplotlib allows)
        :param kind: line (or whatever matplotlib allows)
        """
        df = pandas.read_csv(csv, sep=",")
        
        df['benchmark'] = df['name'].str.split("/", expand=True)[0]
        # df['M'] = df['name'].str.split("/", expand=True)[2]
        if df['name'].str.split("/", expand=True)[1][0] == "n":
            df['benchmark'] = df['benchmark'] + "/" + df['name'].str.split("/", expand=True)[3]
            index_col = "matrix height"
            df[index_col] = df['name'].str.split("/", expand=True)[2].astype(int)
        else:
            df['N'] = df['name'].str.split("/", expand=True)[1].astype(int)
        if "threads" in df:
            df['benchmark'] = df['benchmark']  + "/" + df["threads"].replace(np.nan, 1).astype(int).astype(str)
        if "x" in df:
            df['benchmark'] = df['benchmark']  + "/" + df["x"].replace(np.nan, 1).astype(int).astype(str)
        #if "n_fl_ops" in df:
        #    df['flops/s'] = (df["n_fl_ops"] / df['avg_overall_time'])
        if "bytes_processed" in df:
            df['GB/s'] = (df["bytes_processed"] / df['avg_overall_time'])
        # df['itsps'] = df["x"] * df["y"] * df[index_col] / df['real_time']
        df['avg_overall_time'] /= 1e9

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        
        # ax.set_ylabel("Giterations/s")
        #df_fiber = df[df['benchmark'].str.contains('fiber')]
        #df_fiber = df_fiber.pivot_table(index=index_col, columns='benchmark', values='avg_overall_time')
        #df_split = df[df['benchmark'].str.contains('split')]
        #df_split = df_split.pivot_table(index=index_col, columns='benchmark', values='avg_overall_time')
        #if "n_fl_ops" in df:
        #    ax.set_ylabel("Throughput [GFlop/s]")
        #    df = df.pivot_table(index=index_col, columns='benchmark', values='flops/s')
        #    ax.hlines(xmin=-0.5, xmax=3.5, y=169.9, color='k', linestyles="dashed")
        if 'bytes_processed' in df:
            ax.set_ylabel("Throughput [GB/s]")
            df = df.pivot_table(index=index_col, columns='benchmark', values='GB/s')
            ax.hlines(xmin=-0.5, xmax=4.5, y=18.5, color='k', linestyles="dashed")
        else:
            ax.set_ylabel("time [s]")
            df = df.pivot_table(index=index_col, columns='benchmark', values='avg_overall_time')

        #df_split.plot(kind=kind, ax=ax, colormap="tab20")
        #df_fiber.plot(kind=kind, ax=ax, colormap="Set2")
        df.plot(kind=kind, ax=ax, colormap="tab20")
        
        if show_all_ticks:
            ax.set_xticks([x for x in df_split.index.to_list()])
            ax.set_xticklabels(df_split.index.to_list(), rotation=90)
            # fig.subplots_adjust(bottom=0.2)
        plt.tight_layout()
        
        if kind == 'line':
            set_markers_for_axis(ax)

        plt.savefig(csv + ".pdf")

    @staticmethod
    def plot_minstructor(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', fig_size = (11,5), title = ""):
        """
        Plots a minstructor benchmark csv file.

        :param file: Input file.
        """
        df = pandas.read_csv(csv, index_col=index_col)
        # df = df.drop(columns="data-file-path")
        df['Throughput[GB/s]'] = (df['bytes'] / df['median']) / 1e9
        df = df.pivot_table(index=index_col, columns='type', values='Throughput[GB/s]')

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        ax.set_ylabel("Throughput[GB/s]")
        ax.hlines(xmin=-0.5, xmax=3.5, y=18.5, color='k', linestyles="dashed")
        df.plot(kind=kind, ax=ax, colormap="tab20", title=title)
        
        if kind == 'line':
            set_markers_for_axis(ax)
        plt.tight_layout()
        
        plt.savefig(csv + ".pdf")
        
    @staticmethod
    def plot_sycl_bench(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', fig_size = (11,5), title = ""):
        """
        Plots a benchmark csv file.

        :param file: Input file.
        """
        size_map = {"int32": 4, "int64": 8, "fp32":4, "fp64":8}
        df = pandas.read_csv(csv)
        df["dtype"] = df["Benchmark name"]
        # df = df.drop(columns="data-file-path")
        df['bytes'] = df.apply(lambda row: size_map[row['dtype']] * row['problem-size'], axis=1)
        df['Throughput[GB/s]'] = (df['bytes'] / df['run-time-median']) / 1e9
        df = df.pivot_table(index=index_col, columns='pipeline', values='Throughput[GB/s]')

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        ax.set_ylabel("Throughput[GB/s]")
        ax.hlines(xmin=-0.5, xmax=3.5, y=18.5, color='k', linestyles="dashed")
        df.plot(kind=kind, ax=ax, colormap="tab20", title=title)
        
        if kind == 'line':
            set_markers_for_axis(ax)
        plt.tight_layout()
        
        plt.savefig(csv + ".pdf")
        
    @staticmethod
    def plot_prk(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', fig_size = (11,5), title = "", MF : bool = False):
        """
        Plots a minstructor benchmark csv file.

        :param file: Input file.
        """
        df = pandas.read_csv(csv, index_col=index_col)
        # df = df.drop(columns="data-file-path")
        # df['Throughput[GB/s]'] = (df['bytes'] / df['median']) / 1e9
        if MF:
            df['mfs'] /= 1e3
            df = df.pivot_table(index=index_col, columns='pipeline', values='mfs')
        else:
            df = df.pivot_table(index=index_col, columns='pipeline', values='average')

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        if MF:
            ax.set_ylabel("Throughput [GF/s]")
            ax.hlines(xmin=-0.5, xmax=1.5, y=37.9, color='k', linestyles="dashed")
        else:
            ax.set_ylabel("Mean [s]")
        df.plot(kind=kind, ax=ax, colormap="tab20", title=title)
        
        if kind == 'line':
            set_markers_for_axis(ax)
        plt.tight_layout()

        if MF:
            csv += ".flops"
        plt.savefig(csv + ".pdf")

    @staticmethod
    def plot_babel_stream(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', fig_size = (11,5), title = "", throughput : bool = False):
        """
        Plots a benchmark csv file.

        :param file: Input file.
        """
        df = pandas.read_csv(csv, index_col=index_col)
        if throughput:
            df["throughput"] = df['n_elements'] * df['sizeof'] / df['avg_runtime'] / 1e9
            df = df.pivot_table(index=index_col, columns='pipeline', values='throughput')
        else:
            df = df.pivot_table(index=index_col, columns='pipeline', values='avg_runtime')

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        if throughput:
            ax.set_ylabel("Throughput [GB/s]")
            ax.hlines(xmin=-0.5, xmax=0.5, y=18.5, color='k', linestyles="dashed")
        else:
            ax.set_ylabel("Mean [s]")
        df.plot(kind=kind, ax=ax, colormap="tab20", title=title)
        
        if kind == 'line':
            set_markers_for_axis(ax)
        plt.tight_layout()

        if throughput:
            csv += ".flops"
        plt.savefig(csv + ".pdf")


    @staticmethod
    def plot_sycl_blas(csv : str, index_col : str = "N", xscale : str = 'linear', yscale : str = 'linear', kind : str = 'line', show_all_ticks : bool = False, fig_size = (11,5)):
        """
        Plots a google benchmark csv file (sycl blas).
        
        :param file: Input file.
        :param index_col: The name of the captured C++ variable to use for the x axis. Can also be a list (e.g. "['numBlocks','threadsPerBlock']").
        :param xscale: linear or log (or whatever matplotlib allows)
        :param yscale: linear or log (or whatever matplotlib allows)
        :param kind: line (or whatever matplotlib allows)
        """
        df = pandas.read_csv(csv, sep=",")
        
        df['benchmark'] = df['name'].str.split("/", expand=True)[0]
        
        # only non-transposed
        df = df[df['name'].str.split("/", expand=True)[1] == "n"]
        
        df['benchmark'] = df['benchmark'] + "/" + df['name'].str.split("/", expand=True)[3]
        index_col = "matrix height"
        df[index_col] = df['name'].str.split("/", expand=True)[2].astype(int)

        if "bytes_processed" in df:
            df['GB/s'] = (df["bytes_processed"] / df['avg_overall_time'])
        df['avg_overall_time'] /= 1e9

        plt.figure(figsize=fig_size)
        ax = plt.gca()
        ax.set_xscale(xscale)
        ax.set_yscale(yscale)
        
        if 'bytes_processed' in df:
            ax.set_ylabel("Throughput [GB/s]")
            df = df.pivot_table(index=index_col, columns='pipeline', values='GB/s')
            ax.hlines(xmin=-0.5, xmax=4.5, y=18.5, color='k', linestyles="dashed")
        else:
            ax.set_ylabel("time [s]")
            df = df.pivot_table(index=index_col, columns='pipeline', values='avg_overall_time')

        df.plot(kind=kind, ax=ax, colormap="tab20")
        
        if show_all_ticks:
            ax.set_xticks([x for x in df_split.index.to_list()])
            ax.set_xticklabels(df_split.index.to_list(), rotation=90)
        plt.tight_layout()
        
        if kind == 'line':
            set_markers_for_axis(ax)

        plt.savefig(csv + ".pdf")
if __name__ == "__main__":
    fire.Fire(cli)

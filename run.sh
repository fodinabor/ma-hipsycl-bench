#! /bin/bash -x

org_pwd=`pwd`
export SYCL_SDK_DIR=${org_pwd}/install
export HIPSYCL_DEBUG_LEVEL=1

sudo ${org_pwd}/make_stable_perf.sh
shield="sudo cset shield -c 2-15 -k on"
execute_shielded="sudo -E LD_LIBRARY_PATH=$LD_LIBRARY_PATH cset shield --exec --"
unshield="sudo cset shield -r"

echo -e "\e[1;92minstall current hipSYCL\e[0m"
cd hipSYCL/build
hipSYCL_hash=`git rev-parse --short HEAD`
ninja install
cd $org_pwd

echo -e "\e[1;92msycl-bench\e[0m"
# cd ${org_pwd}/sycl-bench/build-fibers
# ninja clean

# echo -e "  \e[1;94msycl-bench/reduction with fibers\e[0m"
# ninja reduction
# ./reduction --size=307200 --output=${org_pwd}/res/sycl-bench-red-fibers-${hipSYCL_hash}.csv --num-runs=10 --no-verification
# ninja clean

cd ${org_pwd}/sycl-bench/build
ninja clean

echo -e "  \e[1;94msycl-bench/reduction with org\e[0m"
HIPSYCL_PIPELINE=org ninja reduction
$shield
$execute_shielded ./reduction --size=307200 --output=${org_pwd}/res/sycl-bench-red-org-${hipSYCL_hash}.csv --num-runs=10 --no-verification
$unshield
ninja clean

echo -e "  \e[1;94msycl-bench/reduction with pocl\e[0m"
HIPSYCL_PIPELINE=pocl ninja reduction 
$shield
$execute_shielded ./reduction --size=307200 --output=${org_pwd}/res/sycl-bench-red-pocl-${hipSYCL_hash}.csv --num-runs=10 --no-verification
$unshield
ninja clean

echo -e "  \e[1;94msycl-bench/reduction with cbs\e[0m"
HIPSYCL_PIPELINE=cbs ninja reduction
$shield
$execute_shielded ./reduction --size=307200 --output=${org_pwd}/res/sycl-bench-red-cbs-${hipSYCL_hash}.csv --num-runs=10 --no-verification
$unshield
ninja clean

echo -e "\e[1;92msycl-blas\e[0m"
# cd ${org_pwd}/sycl-blas/build-fibers
# ninja clean

# echo -e "  \e[1;94msycl-blas/gemv with fibers\e[0m"
# ninja bench_gemv
# ./benchmark/syclblas/bench_gemv --csv-param=../gemv_params.csv --benchmark_out=${org_pwd}/res/sycl-blas-gemv-fibers-${hipSYCL_hash}.csv --benchmark_out_format=csv
# ninja clean

cd ${org_pwd}/sycl-blas/build
ninja clean

echo -e "  \e[1;94msycl-blas/gemv with org\e[0m"
HIPSYCL_PIPELINE=org ninja bench_gemv
$shield
$execute_shielded ./benchmark/syclblas/bench_gemv --csv-param=../gemv_params.csv --benchmark_out=${org_pwd}/res/sycl-blas-gemv-org-${hipSYCL_hash}.csv --benchmark_out_format=csv
$unshield
ninja clean

echo -e "  \e[1;94msycl-blas/gemv with pocl\e[0m"
HIPSYCL_PIPELINE=pocl ninja bench_gemv
$shield
$execute_shielded ./benchmark/syclblas/bench_gemv --csv-param=../gemv_params.csv --benchmark_out=${org_pwd}/res/sycl-blas-gemv-pocl-${hipSYCL_hash}.csv --benchmark_out_format=csv
$unshield
ninja clean

echo -e "  \e[1;94sycl-blas/gemv with cbs\e[0m"
HIPSYCL_PIPELINE=cbs ninja bench_gemv
$shield
$execute_shielded ./benchmark/syclblas/bench_gemv --csv-param=../gemv_params.csv --benchmark_out=${org_pwd}/res/sycl-blas-gemv-cbs-${hipSYCL_hash}.csv --benchmark_out_format=csv
$unshield
ninja clean

echo -e "\e[1;92mPRK dgemm\e[0m"
cd ${org_pwd}/PRK/Cxx11
rm dgemm-sycl

# echo -e "  \e[0;94mPRK/dgemm-sycl with fibers\e[0m"
# export EXTRA_FLAGS=
# make dgemm-sycl
# ./dgemm-sycl 20 1024 32 > ${org_pwd}/res/prk-dgemm-1024-fibers-${hipSYCL_hash}.txt
# ./dgemm-sycl 5 4096 32 > ${org_pwd}/res/prk-dgemm-4096-fibers-${hipSYCL_hash}.txt
# rm dgemm-sycl

export EXTRA_FLAGS=-DHIPSYCL_NO_FIBERS

#echo -e "  \e[0;94mPRK/dgemm-sycl with org\e[0m"
#HIPSYCL_PIPELINE=org make dgemm-sycl
#./dgemm-sycl 20 1024 32 > ${org_pwd}/res/prk-dgemm-1024-org-${hipSYCL_hash}.txt
#./dgemm-sycl 5 4096 32 > ${org_pwd}/res/prk-dgemm-4096-org-${hipSYCL_hash}.txt
#rm dgemm-sycl

echo -e "  \e[0;94mPRK/dgemm-sycl with pocl\e[0m"
HIPSYCL_PIPELINE=pocl make dgemm-sycl
$shield
$execute_shielded ./dgemm-sycl 20 1024 32 > ${org_pwd}/res/prk-dgemm-1024-pocl-${hipSYCL_hash}.txt
$execute_shielded ./dgemm-sycl 5 4096 32 > ${org_pwd}/res/prk-dgemm-4096-pocl-${hipSYCL_hash}.txt
$unshield
rm dgemm-sycl

echo -e "  \e[0;94mPRK/dgemm-sycl with cbs\e[0m"
HIPSYCL_PIPELINE=cbs make dgemm-sycl
$shield
$execute_shielded ./dgemm-sycl 20 1024 32 > ${org_pwd}/res/prk-dgemm-1024-cbs-${hipSYCL_hash}.txt
$execute_shielded ./dgemm-sycl 5 4096 32 > ${org_pwd}/res/prk-dgemm-4096-cbs-${hipSYCL_hash}.txt
$unshield
rm dgemm-sycl

echo -e "\e[1;92mBabelStream dot\e[0m"
cd ${org_pwd}/BabelStream
rm sycl-stream
export COMPILER=HIPSYCL
export TARGET=CPU
export EXTRA_FLAGS=

# echo -e "  \e[1;94mBabelStream/dot with fibers\e[0m"
# make -f SYCL.make
# ./sycl-stream --csv > ${org_pwd}/res/BabelStream-fibers-${hipSYCL_hash}.csv
# rm sycl-stream

export EXTRA_FLAGS=-DHIPSYCL_NO_FIBERS

echo -e "  \e[1;94mBabelStream/dot with org\e[0m"
HIPSYCL_PIPELINE=org make -f SYCL.make
$shield
$execute_shielded ./sycl-stream --csv > ${org_pwd}/res/BabelStream-org-${hipSYCL_hash}.csv
$unshield
rm sycl-stream

echo -e "  \e[1;94mBabelStream/dot with pocl\e[0m"
HIPSYCL_PIPELINE=pocl make -f SYCL.make
$shield
$execute_shielded ./sycl-stream --csv > ${org_pwd}/res/BabelStream-pocl-${hipSYCL_hash}.csv
$unshield
rm sycl-stream

echo -e "  \e[1;94mBabelStream/dot with cbs\e[0m"
HIPSYCL_PIPELINE=cbs make -f SYCL.make
$shield
$execute_shielded ./sycl-stream --csv > ${org_pwd}/res/BabelStream-cbs-${hipSYCL_hash}.csv
$unshield
rm sycl-stream

cd ${org_pwd}

sudo ${org_pwd}/reset_stable_perf.sh

./plotting/generate_plots.sh ${hipSYCL_hash}

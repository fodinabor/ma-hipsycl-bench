#! /bin/bash

for i in {0..15}
do
  echo 1 > /sys/devices/system/cpu/cpu$(($i+16))/online
  echo ondemand > /sys/devices/system/cpu/cpu${i}/cpufreq/scaling_governor
done

# cset shield -r

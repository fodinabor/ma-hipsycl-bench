#! /bin/bash

for i in {0..15}
do
  echo 0 > /sys/devices/system/cpu/cpu$(($i+16))/online
  echo performance > /sys/devices/system/cpu/cpu${i}/cpufreq/scaling_governor
done
